import Header from './components/Header';
import {useState, Fragment} from 'react';
import Footer from './components/Footer';
import Producto from './components/Producto';
import Changuito from './components/Changuito';

function App() {

// Listado de productos con un state

const [productos, guardarProductos] = useState([
  {id:1, articulo:"Guitarra criolla", precio:12500},
  {id:2, articulo:"Guitarra eléctrica", precio:72500},
  {id:3, articulo:"Bajo Jazz Bass", precio:215000},
  {id:4, articulo:"Batería Mapex", precio:102000},
  {id:5, articulo:"Micrófono Sure 58", precio:75000}
]);

// State del changuito

const [changuito, agregarProducto] = useState([]);

  return (
    <Fragment>
      <Header />
      <h1>Instrumentos Musicales SA - Compre online</h1>
      {productos.map(producto => 
       (
         <Producto 
          key= {producto.id}
          producto = {producto}
          productos = {productos}
          changuito = {changuito}
          agregarProducto = {agregarProducto}
         />
       )
        )}

      <Changuito
        changuito = {changuito}
        agregarProducto = {agregarProducto}
      />

      <Footer 
        anio={"2021"}
      />
    </Fragment> 
  );
}

export default App;
