import React, { Fragment } from 'react';

const Producto = ({producto, productos, changuito, agregarProducto}) => {

// Extraigo los valores

const {id, articulo, precio} = producto;

// Función para agregar productos al changuito

const seleccionarProducto = (id) => {
        const producto = productos.filter (
            producto => producto.id === id)[0];
            agregarProducto([
                ...changuito,
                producto
            ]);
        console.log(producto);
};


// Función para eliminar un producto del changuito

const eliminarProducto = (id) => {
    const productos = changuito.filter (
        producto => producto.id !== id);

    // Actualizar el state
        agregarProducto (productos);
};

    return ( 
        <Fragment>
            <div>
                <h3>{id} {articulo} {precio} </h3>
                
                {
                    productos 

                    ? 
                    <button
                        type="button"
                        onClick={ () => seleccionarProducto(id) }
                    >Comprar</button>
                    :
                    <button
                        type="button"
                        onClick = { () => eliminarProducto(id)}
                   >Eliminar</button>
                }
                
                
            </div>
            
        </Fragment>
        
     );
}
 
export default Producto;
